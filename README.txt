THIS GIT REPO HAS BEEN MOVED TO THE CONNECT2ID TEAM


The Nimbus OAuth 2.0 SDK for Java with OpenID Connect extensions has been moved
to 

https://bitbucket.org/connect2id/oauth-2.0-sdk-with-openid-connect-extensions

Please, update your bookmarks and git links accordingly.


2014-02-12
